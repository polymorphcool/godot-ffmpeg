/*
 *  
 *  
 *  _________ ____  .-. _________/ ____ .-. ____ 
 *  __|__  (_)_/(_)(   )____<    \|    (   )  (_)
 *                  `-'                 `-'      
 *  
 *  
 *  art & game engine
 *  
 *  ____________________________________  ?   ____________________________________
 *                                      (._.)
 *  
 *  
 *  This file is part of Ffmpeg addon
 *  For the latest info, see http://polymorph.cool/
 *  
 *  Copyright (c) 2020 polymorph.cool
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *  
 *  ___________________________________( ^3^)_____________________________________
 *  
 *  ascii font: rotated by MikeChat & myflix
 *  have fun and be cool :)
 *  
 * 
 */

#include "fast_forward_loader.h"
#include "fast_forward_stream.h"
#include "register_types.h"


static Ref<FastForwardLoader> resource_loader_generic;

void register_ffmpeg_types() {

	// ffmpeg configuration
	av_log_set_flags(AV_LOG_SKIP_REPEATED);
	avcodec_register_all();

	resource_loader_generic.instance();
	ResourceLoader::add_resource_format_loader(resource_loader_generic, true);
	ClassDB::register_class<FastForwardStream>();

//#ifndef _3D_DISABLED
//    ClassDB::register_class<FutariParticles>();
//#ifdef TOOLS_ENABLED
//   EditorPlugins::add_by_type<EditorPluginFutari>();
//#endif
//#endif

}

void unregister_ffmpeg_types() {

	ResourceLoader::remove_resource_format_loader(resource_loader_generic);
	resource_loader_generic.unref();

}
