/*
 * video_stream_generic.cpp
 *
 *  Created on: Sep 29, 2020
 *      Author: frankiezafe
 */

#include "fast_forward_stream.h"

FastForwardStream::FastForwardStream(){

}

FastForwardStream::~FastForwardStream() {
}

Ref<VideoStreamPlayback> FastForwardStream::instance_playback() {

	if ( player.is_valid() ) {
		player.unref();
	}

	player.instance();
	player->load(file);
	return player;

//	pb = (Ref<VideoStreamPlaybackGeneric>) memnew(VideoStreamPlaybackGeneric);
//	pb->set_audio_track(audio_track);
//	pb->set_file(file);
//	instanciated = true;
//	return pb;

}

void FastForwardStream::set_file(const String &p_file) {
	file = p_file;
}

String FastForwardStream::get_file() {
	return file;
}

void FastForwardStream::seek(const real_t &p ) {
	if (player.is_null()) {
		return;
	}
	player->seek( p );
}

real_t FastForwardStream::get_duration() {
	if (!player.is_valid()) {
		return 0;
	}
	return player->get_length();
}

void FastForwardStream::set_audio_track(int p_track) {}

Ref<Texture> FastForwardStream::get_texture() {
	return player->get_vid_texture();
}

void FastForwardStream::update(float p_delta) {

}

void FastForwardStream::_bind_methods() {

	ClassDB::bind_method(D_METHOD("set_file", "file"), &FastForwardStream::set_file);
	ClassDB::bind_method(D_METHOD("get_file"), &FastForwardStream::get_file);
	ClassDB::bind_method(D_METHOD("seek"), &FastForwardStream::seek);
	ClassDB::bind_method(D_METHOD("get_duration"), &FastForwardStream::get_duration);

//	ADD_PROPERTY(
//			PropertyInfo(Variant::BOOL, "decode_audio", PROPERTY_HINT_NONE, "",
//					PROPERTY_USAGE_NOEDITOR | PROPERTY_USAGE_INTERNAL),
//			"set_decode_audio",
//			"get_decode_audio");

	ADD_PROPERTY(
			PropertyInfo(Variant::STRING, "file", PROPERTY_HINT_NONE, "",
					PROPERTY_USAGE_NOEDITOR | PROPERTY_USAGE_INTERNAL),
			"set_file",
			"get_file");

}
